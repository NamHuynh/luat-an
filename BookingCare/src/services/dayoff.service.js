import axios from 'axios';
import Constants from '@/utils/constants';

export const DayOffService = {
    getDayOffInDate,
    getDayOffInDoctor,
    postDayOff,
    deleteDayOff,
};


function getDayOffInDate(date,doctorId) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/day_offs?filter={"where":{"date":"${date}","doctorId":"${doctorId}"}}`,
    });
}

function getDayOffInDoctor(doctorId) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/day_offs?filter={"where":{"doctorId":"${doctorId}"}}`,
    });
}

function postDayOff(dataDayOff) {
    const data = new URLSearchParams();
    data.append('date',dataDayOff.date );
    data.append('time_start',dataDayOff.time_start );
    data.append('time_end',dataDayOff.time_end );
    data.append('doctorId',dataDayOff.doctorId );
    data.append('reason',dataDayOff.reason );
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/day_offs`,
        data: data,
    });
}

function deleteDayOff(idDayOff) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/day_offs/${idDayOff}`,
    });
}