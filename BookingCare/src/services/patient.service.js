import axios from 'axios';
import Constants from '@/utils/constants';

export const patientService = {
    getPatient,
    updatePatient,
    postBooking,
    getBookingPatient,
    getAllPatient,
    deletePatient,
    updatePatientOfAdmin,
    searchPatient,
    postPatient,
    checkEmail
};
function getAllPatient() {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/patients`,
    });
}
function getPatient(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/patients/${id}`,
    });
}

function updatePatientOfAdmin(dataPatient) {
    const data = new URLSearchParams();
    data.append('id', dataPatient.id);
    data.append('name', dataPatient.name);
    data.append('phone', dataPatient.phone);
    data.append('sex', dataPatient.sex);
    data.append('email', dataPatient.email);
    data.append('province', dataPatient.province);
    data.append('district', dataPatient.district);
    data.append('location_detail', dataPatient.location_detail);
    data.append('old', dataPatient.old);
    data.append('password', dataPatient.password);
    return axios({
        method: 'PUT',
        url: `${Constants.SERVER}/patients`,
        data: data,
    });
}

function updatePatient(dataPatient) {
    const data = new URLSearchParams();
    data.append('id', dataPatient.id);
    data.append('name', dataPatient.name);
    data.append('phone', dataPatient.phone);
    data.append('sex', dataPatient.sex);
    data.append('email', dataPatient.email);
    data.append('province', dataPatient.province);
    data.append('district', dataPatient.district);
    data.append('location_detail', dataPatient.location_detail);
    data.append('old', dataPatient.old);
    data.append('password', localStorage.getItem("token"));
    return axios({
        method: 'PUT',
        url: `${Constants.SERVER}/patients`,
        data: data,
    });
}

// delete
function postBooking(patientId, scheduleId, doctorId) {
    const data = new URLSearchParams();
    data.append('scheduleId', scheduleId);
    data.append('doctorId', doctorId);
    data.append('patientId', patientId);
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/patients/${patientId}/bookings`,
        data: data,
    });
}

function getBookingPatient(patientId) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/patients/${patientId}/bookings`,
    });
}

function deletePatient(patientId) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/patients/${patientId}`,
    });
}

function searchPatient(textName) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/patients/searchName?name=${textName}`,
    });
}

function postPatient(dataUser){
    const data = new URLSearchParams();
    data.append('name', dataUser.name);
    data.append('phone', dataUser.phone);
    data.append('sex', dataUser.sex);
    data.append('email', dataUser.email);
    data.append('province', dataUser.province);
    data.append('district', dataUser.district);
    data.append('location_detail', dataUser.location_detail);
    data.append('old', dataUser.old);
    data.append('password', dataUser.password);

    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/patients`,
        data: data,
    });
}

function checkEmail(email) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/patients/CheckEmail?email=${email}`,
    });
}