import axios from 'axios';
import Constants from '@/utils/constants';

export const adminService = {
    addAdmin,
};

function addAdmin(dataAdmin) {
    const data = new URLSearchParams();
    data.append('name', dataAdmin.name);
    data.append('email', dataAdmin.email);
    data.append('password', dataAdmin.password);
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/admins`,
        data: data,
    });
}