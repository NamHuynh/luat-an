﻿import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/TheHome.vue"
import Blank from "@/views/Blank.vue";
import RegisterSingle from "@/views/RegisterSingle.vue";
import ListSelectMedicalSpecialty from "@/views/ListSelectMedicalSpecialty.vue";
import ListDoctor from "@/views/ListDoctor.vue";
import ProfileUser from "@/views/ProfileUser.vue";
import Scheduling from "@/views/Scheduling";
import Login from "@/views/LoginViewer"
import ManageViewer from "../views/ManageViewer";
import WaitingSchedule from "../views/WaitingSchedule";
import ProfileDoctor from "../views/ProfileDoctor";

import DashboardLayout from "../components/layout/DashboardLayout";
import UserProfile from "../views/Admin/UserProfile";
import ListPatientBooked from "../views/Admin/ListPatientBooked";
import Schedule from "../views/Admin/Schedule";
import SettingSchedule from "../views/Admin/SettingSchedule";
import Notifications from "../views/Admin/Notifications";

import OverViewBooking from "../views/Patient/OverViewBooking";
import DoctorBooking from "../views/Patient/DoctorBooking";
import DoctorBookingSpecialist from "../views/Patient/DoctorBookingSpecialist";
import SpecialistBooking from "../views/Patient/SpecialistBooking";
import BookingLayout from "../components/LayoutBooking/BookingLayout";
import EnterInfo from "../views/Patient/EnterInfo";
import ConfirmBooking from "../views/Patient/ConfirmBooking";
import DetailNotify from "../views/Patient/DetailNotify";
import DetailInfoDoctor from "../views/Patient/DetailInfoDoctor";
import FollowUpExamination from "../views/Patient/FollowUpExamination";

import AdminLayout from "../components/layoutAdmin/AdminLayout";
import ManageDoctor from "../views/AdminManage/ManageDoctor";
import DetailManageDoctor from "../views/AdminManage/DetailManageDoctor";
import ManagePatient from "../views/AdminManage/ManagePatient";
import ManageSpecialist from "../views/AdminManage/ManageSpecialist";
import CreateAccount from "../views/AdminManage/CreateAccount";

// import Tutorial from "@/views/Location.vue";
Vue.use(VueRouter);

// TODO Web Template Studio: Add routes for your new pages here.
const router = new VueRouter({
// export const router = new VueRouter({
// export default new VueRouter({
  mode: "history",
  // base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      alias: '/home',
      name: 'home',
      component: Home,
      meta: {
          title: 'Homepage',
      }
    },
    {
      path: '/blank',
      alias: '/blank',
      name: 'blank',
      component: Blank,
      meta: {
        title: 'Blank',
      }
    },


    {
      path: '/doctor',
      component: DashboardLayout,
      redirect: '/doctor/user',
      children: [
        {
          path: 'user',
          name: 'User',
          component: UserProfile,
          meta: {
            requiresAuth: true,
            requiresDoctor: true,
          },
        },
        {
          path: 'list-patient-booked',
          name: 'List Patient Booked',
          component: ListPatientBooked,
          meta: {
            requiresAuth: true,
            requiresDoctor: true,
          },

        },
        {
          path: 'schedule',
          name: 'Schedule',
          component: Schedule,
          meta: {
            requiresAuth: true,
            requiresDoctor: true,
          },
        },
        {
          path: 'setting-schedule',
          name: 'Setting Schedule',
          component: SettingSchedule,
          meta: {
            requiresAuth: true,
            requiresDoctor: true,
          },
        },
        {
          path: 'notifications',
          name: 'Notifications',
          component: Notifications,
          meta: {
            requiresAuth: true,
            requiresDoctor: true,
          },
        },
      ]
    },

    {
      path: '/admin',
      component: AdminLayout,
      redirect: '/admin/manage-doctor',
      children: [
        {
          path: 'manage-doctor',
          name: 'Manage Doctor',
          component: ManageDoctor,
          meta: {
            requiresAuth: true,
            requiresAdmin: true,
          },
        },
        {
          path: 'detail-manage-doctor/:id',
          name: 'Detail Manage Doctor',
          component: DetailManageDoctor,
          meta: {
            requiresAuth: true,
            requiresAdmin: true,
          },
        },
        {
          path: 'manage-patient',
          name: 'Manage Patient',
          component: ManagePatient,
          meta: {
            requiresAuth: true,
            requiresAdmin: true,
          },
        },
        {
          path: 'manage-specialist',
          name: 'Manage Specialist',
          component: ManageSpecialist,
          meta: {
            requiresAuth: true,
            requiresAdmin: true,
          },
        },
        {
          path: 'create-account',
          name: 'Create Account',
          component: CreateAccount,
          meta: {
            requiresAuth: true,
            requiresAdmin: true,
          },
        },

      ]
    },


    {
      path: '/booking',
      component: BookingLayout,
      redirect: '/booking/doctor-booking',
      children: [
        {
          path: 'overview',
          name: 'OverView',
          component: OverViewBooking,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'doctor-booking',
          name: 'Doctor Booking',
          component: DoctorBooking,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'doctor-booking-specialist/:id',
          name: 'Doctor Booking Specialist',
          component: DoctorBookingSpecialist,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'specialist-booking',
          name: 'Specialist Booking',
          component: SpecialistBooking,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'enter-info/:id',
          name: 'Enter Info',
          component: EnterInfo,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'confirm-booking',
          name: 'Confirm Booking',
          component: ConfirmBooking,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'detail-notify',
          name: 'Detail Notify',
          component: DetailNotify,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'detail-info-doctor/:id',
          name: 'Detail Info Doctor',
          component: DetailInfoDoctor,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'profile-user',
          name: 'Profile User',
          component: ProfileUser,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          }
        },
        {
          path: 'follow-up-examination',
          name: 'Follow Up Examination',
          component: FollowUpExamination,
          meta: {
            title: 'Booking',
            requiresAuth: true,
            requiresPatient: true,
          },
        },


      ]
    },


    {
      path: '/login',
      alias: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: 'Login',
      },
    },
    {
      path: '/registerSingle',
      alias: '/registerSingle',
      name: 'registerSingle',
      component: RegisterSingle,
      meta: {
        title: 'RegisterSingle',
      }
    },
    {
      path: '/listSelectMedicalSpecialty',
      alias: '/listSelectMedicalSpecialty',
      name: 'listSelectMedicalSpecialty',
      component: ListSelectMedicalSpecialty,
      meta: {
        title: 'ListSelectMedicalSpecialty',
        requiresAuth: true,
      }
    },
    {
      path: '/manageViewer',
      alias: '/manageViewer',
      name: 'manageViewer',
      component: ManageViewer,
      meta: {
        title: 'ManageViewer',
        requiresAuth: true,
        requiresAdmin: true,
      }
    },
    {
      path: '/listDoctor',
      alias: '/listDoctor',
      name: 'listDoctor',
      component: ListDoctor,
      meta: {
        title: 'ListDoctor',
        requiresAuth: true,
      }
    },
    {
      path: '/waitingSchedule',
      alias: '/waitingSchedule',
      name: 'waitingSchedule',
      component: WaitingSchedule,
      meta: {
        title: 'WaitingSchedule',
        requiresAuth: true,
        requiresPatient: true,
      }
    },
    {
      path: '/listDoctor/:id',
      alias: '/listDoctor/:id',
      name: 'listDoctorHasId',
      component: ListDoctor,
      meta: {
        title: 'ListDoctor',
        requiresAuth: true,
      }
    },
    {
      path: '/profileUser',
      alias: '/profileUser',
      name: 'profileUser',
      component: ProfileUser,
      meta: {
        title: 'ProfileUser',
        requiresAuth: true,
      }
    },
    {
      path: '/profileDoctor',
      alias: '/profileDoctor',
      name: 'profileDoctor',
      component: ProfileDoctor,
      meta: {
        title: 'profileDoctor',
        requiresAuth: true,
        requiresDoctor: true,
      }
    },
    {
      path: '/scheduling',
      alias: '/scheduling',
      name: 'scheduling',
      component: Scheduling,
      meta: {
        title: 'Scheduling',
        requiresAuth: true,
        requiresDoctor: true,
      }
    },
  ]
});
router.beforeEach((to, from, next) => {
  /**
   * Auth Required:
   * User that already login to system and have *Token* can reach the component
   */
  if (to.meta.requiresAuth) {
    var loggedIn = localStorage.getItem('token');
    let typeUser = localStorage.getItem("typeUser");
    if (!loggedIn) {
      next({path :"/login"});
    }
    else {
      if(to.meta.requiresDoctor){
        if(typeUser !== "Doctor"){
          next({path :"/login"});
        }

      }else if(to.meta.requiresAdmin){
        if(typeUser !== "Admin"){
          next({path :"/login"});
        }
      }else if(to.meta.requiresPatient){
        if(typeUser !== "Patient"){
          next({path :"/login"});
        }
      }
      else{
        next();
      }
    }
  }
  next()
});
export default router;