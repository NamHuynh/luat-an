import axios from 'axios';
import Constants from '@/utils/constants';
export const notifyPatientService = {
    getNotify,
    postNotify,
    putNotify,
    deleteNotify
};

function getNotify(patientId) {
    return axios({
        method: 'GET',

        url: `${Constants.SERVER}/notify_patients?filter={"where":{"patientId":"${patientId}"},"order":["id DESC"]}`,
    });
}
function postNotify(dataNotify) {
    const data = new URLSearchParams();
    data.append('message',dataNotify.message );
    data.append('patientId',dataNotify.patientId );
    data.append('type',dataNotify.type );
    data.append('isRead',false );
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/notify_patients`,
        data: data,
    });
}
function putNotify(dataNotify) {
    const data = new URLSearchParams();
    data.append('isRead',true );
    data.append('message',dataNotify.message );
    data.append('type',dataNotify.type );
    data.append('patientId',dataNotify.patientId );
    return axios({
        method: 'PUT',
        url: `${Constants.SERVER}/notify_patients/${dataNotify.id}`,
        data: data,
    });
}
function deleteNotify(id) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/notify_patients/${id}`,
    });
}