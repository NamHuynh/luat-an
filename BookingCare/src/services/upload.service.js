import axios from 'axios';
import Constants from '@/utils/constants';

export const uploadService = {
    postFileImg,

};

function postFileImg(file) {
    let formData = new FormData();
    formData.append('file',file)
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/uploads/image/upload`,
        data: formData,
    });
}