import axios from 'axios';
import Constants from '@/utils/constants';

export const doctorService = {
    getDoctor,
    getScheduleInDoctor,
    getSearchDoctor,
    getScheduleOfDoctor,
    postDateDefault,
    postTimeInDate,
    editTimeInDate,
    deleteTimeInDate,
    deleteDoctor,
    updateDoctor,
    CreateDoctor,
    getDoctorByID,
    searchDoctor,
    searchNameBySpecialistId,
    getDoctorBySpecialist,
    checkEmail,
    postDoctor,
};
function getDoctor() {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors`,
    });
}
function getDoctorBySpecialist(idSpecialist) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors?filter={"where":{"specialistId":"${idSpecialist}"}}`,
    });
}
function checkEmail(email) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/CheckEmail?email=${email}`,
    });
}
function getDoctorByID(idDoctor) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/${idDoctor}`,
    });
}

function deleteDoctor(id) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/doctors/${id}`,
    });
}
function getScheduleInDoctor(id,date) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/${id}/schedules?filter={"where":{"date":"${date}"}}`,
    });
}

function getSearchDoctor(id,date,nameDoctor) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/${id}/schedules?filter={"where":{"date":"${date}","name":{"like":"${nameDoctor}","options":"i"}}}`,
    });
}

function getScheduleOfDoctor(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/${id}/schedules?filter[order]=id DESC`,
    });
}

function postDateDefault(date) {
    const data = new URLSearchParams();
    data.append('date',date );
    data.append('time_start', "07:30");
    data.append('time_end', "08:00");
    data.append('follow_up_examination', false);
    data.append('limit', 2);
    data.append('doctorId', localStorage.getItem("id"));
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/doctors/${localStorage.getItem("id")}/schedules`,
        data: data,
    });
}

function postTimeInDate(date,timeStart,timeEnd,limit) {
    const data = new URLSearchParams();
    data.append('date',date );
    data.append('time_start', timeStart);
    data.append('time_end', timeEnd);
    data.append('follow_up_examination', false);
    data.append('limit', limit);
    data.append('doctorId', localStorage.getItem("id"));
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/doctors/${localStorage.getItem("id")}/schedules`,
        data: data,
    });
}

function editTimeInDate(dataTimeNew) {
    const data = new URLSearchParams();
    data.append('id', dataTimeNew.id);
    data.append('date', dataTimeNew.date);
    data.append('time_start', dataTimeNew.time_start);
    data.append('time_end', dataTimeNew.time_end);
    data.append('follow_up_examination', false);
    data.append('limit', dataTimeNew.limit);
    data.append('doctorId', localStorage.getItem("id"));
    return axios({
        method: 'PUT',
        url: `${Constants.SERVER}/doctors/${localStorage.getItem("id")}/schedules/${dataTimeNew.id}`,
        data: data,
    });
}

function deleteTimeInDate(idSchedule){
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/doctors/${localStorage.getItem("id")}/schedules/${idSchedule}`,
    });
}

function postDoctor(dataDoctor) {
    const data = new URLSearchParams();
    data.append('name', dataDoctor.name);
    data.append('email', dataDoctor.email);
    data.append('photo', dataDoctor.photo);
    data.append('phone', dataDoctor.phone);
    data.append('summary', dataDoctor.summary);
    data.append('location',  dataDoctor.location);
    data.append('price',  dataDoctor.price);
    data.append('specialistId',  dataDoctor.specialistId);
    data.append('password',  dataDoctor.password);
    data.append('minute',  dataDoctor.minute);
    data.append('time_start_morning',  dataDoctor.time_start_morning);
    data.append('time_end_morning',  dataDoctor.time_end_morning);
    data.append('time_start_afternoon',  dataDoctor.time_start_afternoon);
    data.append('time_end_afternoon',  dataDoctor.time_end_afternoon);
    data.append('check',  dataDoctor.check);
    data.append('HH',  dataDoctor.HH);
    data.append('HV',  dataDoctor.HV);
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/doctors`,
        data: data,
    });
}

function updateDoctor(dataDoctor) {
    const data = new URLSearchParams();
    data.append('name', dataDoctor.name);
    data.append('email', dataDoctor.email);
    data.append('photo', dataDoctor.photo);
    data.append('phone', dataDoctor.phone);
    data.append('summary', dataDoctor.summary);
    data.append('location',  dataDoctor.location);
    data.append('price',  dataDoctor.price);
    data.append('specialistId',  dataDoctor.specialistId);
    data.append('password',  dataDoctor.password);
    data.append('minute',  dataDoctor.minute);
    data.append('time_start_morning',  dataDoctor.time_start_morning);
    data.append('time_end_morning',  dataDoctor.time_end_morning);
    data.append('time_start_afternoon',  dataDoctor.time_start_afternoon);
    data.append('time_end_afternoon',  dataDoctor.time_end_afternoon);
    data.append('check',  dataDoctor.check);
    data.append('HH',  dataDoctor.HH);
    data.append('HV',  dataDoctor.HV);
    return axios({
        method: 'PUT',
        url: `${Constants.SERVER}/doctors/${dataDoctor.id}`,
        data: data,
    });
}

function searchDoctor(textName) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/searchName?name=${textName}`,
    });
}
function searchNameBySpecialistId(idSpecialist, name) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors/searchNameBySpecialistId?name=${name}&specialistId=${idSpecialist}`,
    });
}


function CreateDoctor(dataDoctor) {
    const data = new URLSearchParams();
    data.append('name', dataDoctor.name);
    data.append('email', dataDoctor.email);
    data.append('photo', dataDoctor.photo);
    data.append('phone', dataDoctor.phone);
    data.append('summary', dataDoctor.summary);
    data.append('training_process', dataDoctor.training_process);
    data.append('research_works', dataDoctor.research_works);
    data.append('treatment_diseases',  dataDoctor.treatment_diseases);
    data.append('location',  dataDoctor.location);
    data.append('price',  dataDoctor.price);
    data.append('specialistId',  dataDoctor.specialistId);
    data.append('password',  dataDoctor.password);
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/doctors`,
        data: data,
    });
}


