import axios from 'axios';
import Constants from '@/utils/constants';

export const CommentService = {
    getCommentByIdDoctor,
    postComment,
    deleteComment,
    getCommentByIdPatient
};


function getCommentByIdDoctor(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/comments?filter={"where":{"doctorId":"${id}"}}`,
    });
}
function getCommentByIdPatient(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/comments?filter={"where":{"patientId":"${id}"}}`,
    });
}

function deleteComment(id) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/comments/${id}`,
    });
}
function postComment(dataComment) {
    const data = new URLSearchParams();
    data.append('message',dataComment.message );
    data.append('doctorId',dataComment.doctorId );
    data.append('patientId',dataComment.patientId );
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/comments`,
        data: data,
    });
}