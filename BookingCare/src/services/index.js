// export * from './manga.service.js'; example
export * from './doctor.service';
export * from './user.service';
export * from './patient.service';
export * from './specialist.service';
export * from './schedule.service';
export * from './booking.service';
export * from './location.service';
export * from './admin.service';
export * from './upload.service';
export * from './support.service';
export * from './comment.service';
export * from './dayoff.service';
export * from './notify-patient.service';