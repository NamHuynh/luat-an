import axios from 'axios';
import Constants from '@/utils/constants';

export const locationService = {
    getLocation,
    getCity,
    getIdByName
};

function getLocation() {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/provinces`,
    });
}
function getIdByName(name) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/provinces?filter={"where":{"_name": "${name}"}}`,
    });
}
function getCity(idLocation) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/provinces/${idLocation}/districts`,
    });
}