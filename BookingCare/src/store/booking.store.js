export default ({
    namespaced: true,
    state: {
        dataBooking: {},
        numNotify: 0,
    },
    getters: {
    },
    mutations: {
        SET_NUM_NOTIFY: function(state){
            state.numNotify += 1;
        },
        RESET_NUM_NOTIFY: function (state) {
            state.numNotify = 0;
        },
        SET_DATA_BOOKING: function (state,data) {
            state.dataBooking = data;
        }
    },
    actions: {
        setNumNotify: function(context){
            context.commit('SET_NUM_NOTIFY');
        },
        setDataBooking: function(context,data){
            context.commit('SET_DATA_BOOKING',data);
        },
        resetNumNotify: function (context) {
            context.commit('RESET_NUM_NOTIFY');
        }
    }
})