import LoginSingle from "../views/LoginViewer";
import TheHome from "../views/TheHome";
// import AppModal from "../views/AppModal";
export const modalHandler ={
    beforeEnterModalHandler,
};
function beforeEnterModalHandler(to, from, next){
    if (from.matched.length === 0) {
        // from.matched.length = 0 when user reload page
        let defaultComponent = LoginSingle;
        switch (to.name) {
            case "login":
                defaultComponent = LoginSingle;
                break;
            case "manga_info":
                defaultComponent = TheHome;
                break;
        }
        to.matched[0].components = {
            default: defaultComponent,
            modal: false
        };
    } else {
        // when user click to show modal
        if (from.matched.length > 1) {
            const childrenView = from.matched.slice(1, from.matched.length);
            for (let view of childrenView) {
                to.matched.push(view)
            }
        }
        if (to.matched[0].components) {
            to.matched[0].components.default = from.matched[0].components.default;
            // to.matched[0].components.modal = AppModal;
        }

        to.meta.preRoute = from.name;
        to.meta.preParams = from.params;
    }
    next();
}
