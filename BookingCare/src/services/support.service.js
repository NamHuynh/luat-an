import axios from 'axios';
import Constants from '@/utils/constants';

export const supportService = {
    postSupport,
    deleteSupport,
    getSupport
};

function getSupport() {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/supports`,
    });
}
function postSupport(dataSupport) {
    const data = new URLSearchParams();
    data.append('name', dataSupport.name);
    data.append('email', dataSupport.email);
    data.append('phone', dataSupport.phone);
    data.append('message', dataSupport.message);
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/supports`,
        data: data,
    });
}
function deleteSupport(idSupport) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/supports/${idSupport}`,
    });
}

