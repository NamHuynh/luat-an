import axios from 'axios';
import Constants from '@/utils/constants';

export const userService = {
    loginAdmin,
    loginDoctor,
    loginPatient,
    getIdAdmin,
    getIdDoctor,
    getIdPatient,
    checkUser
};

function loginAdmin(email, password) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/admins?filter={"where":{"password":"${password}","email":"${email}"}}`,
    });
}

function loginDoctor(email, password) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors?filter={"where":{"password":"${password}","email":"${email}"}}`,
    });
}

function loginPatient(email, password) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/patients?filter={"where":{"password":"${password}","email":"${email}"}}`,
    });
}
// function loginPatient(email, password) {
//     const data = new URLSearchParams();
//     data.append('email', email);
//     data.append('password', password);
//     return axios({
//         method: 'POST',
//         url: `${Constants.SERVER}/patients/Login`,
//         data: data,
//     });
// }
function getIdAdmin(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors?filter={"where":{"id":"${id}"}}`,
    });
}
function getIdDoctor(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors?filter={"where":{"id":"${id}"}}`,
    });
}
function getIdPatient(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/doctors?filter={"where":{"id":"${id}"}}`,
    });
}
function checkUser(typeUser, email, token) {
    switch (typeUser) {
        case 'Admin':
            return axios({
                method: 'GET',
                url: `${Constants.SERVER}/admins?filter={"where":{"password":"${token}","email":"${email}"}}`,
            });
            break;
        case 'Doctor':
            return axios({
                method: 'GET',
                url: `${Constants.SERVER}/doctors?filter={"where":{"password":"${token}","email":"${email}"}}`,
            });
            break;
        case 'Patient':
            return axios({
                method: 'GET',
                url: `${Constants.SERVER}/patients?filter={"where":{"password":"${token}","email":"${email}"}}`,
            });
            break;
    }
}