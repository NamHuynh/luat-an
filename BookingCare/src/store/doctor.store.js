import {doctorService} from '@/services';
export default ({
    namespaced: true,
    state: {
        listDoctorResponsive: [],
    },
    getters: {
    },
    mutations: {
        SET_LIST_DOCTOR: function (state, componentName) {
            state.modalComponent = componentName;
        }
    },
    actions: {
        setListDoctor: async function (context) {
            let check = true;
            if (context.state.listDoctorResponsive.length == 0) {
                await doctorService.getDoctor()
                    .then(response => {
                        context.commit('SET_LIST_DOCTOR', response.data);
                    })
                    .catch(error => {
                        console.log(error);
                        check = false;
                    })
            }
            return check;
        },
    }
})
