import axios from 'axios';
import Constants from '@/utils/constants';
export const scheduleService = {
    getScheduleById,
    getScheduleInBooking,
    deleteScheduleInBooking
};
function getScheduleById(idSchedule) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/schedules/${idSchedule}`,
    });
}
function getScheduleInBooking(idSchedule) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/schedules/${idSchedule}/bookings`,
    })
}
function deleteScheduleInBooking(idSchedule) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/schedules/${idSchedule}/bookings`,
    });
}


