import axios from 'axios';
import Constants from '@/utils/constants';
export const specialistService = {
    getSpecialist,
    getDoctorInSpecialist,
    countDoctorInSpecialty,
    updateSpecialty,
    postSpecialty,

    getSpecialistById,
    searchSpecialist,
    deleteSpecialist
};

function getSpecialist() {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/specialists`,
    });
}
function getSpecialistById(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/specialists/${id}`,
    });
}

function getDoctorInSpecialist(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/specialists/${id}/doctors`,
    });
}

function countDoctorInSpecialty(idSpecialty) {
        return axios({
            method: 'GET',
            url: `${Constants.SERVER}/specialists/${idSpecialty}/doctors/count`,
        });
}
function updateSpecialty(dataSpeciatly) {
    const data = new URLSearchParams();
    data.append('id', dataSpeciatly.id);
    data.append('name', dataSpeciatly.name);
    data.append('img', dataSpeciatly.img);
    return axios({
        method: 'PUT',
        url: `${Constants.SERVER}/specialists`,
        data: data,
    });
}
// /specialists/3/doctors
function postSpecialty(dataSpeciatly) {
    const data = new URLSearchParams();
    data.append('name', dataSpeciatly.name);
    data.append('img', dataSpeciatly.img);
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/specialists`,
        data: data,
    });
}

function searchSpecialist(textName) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/specialists/searchName?name=${textName}`,
    });
}
function deleteSpecialist(idSpecialist) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/specialists/${idSpecialist}`,
    });
}