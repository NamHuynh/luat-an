import axios from 'axios';
import Constants from '@/utils/constants';
export const bookingService = {
    getBookingByDoctorAnDay,
    postBooking,
    getBooking,
    getBookingByDoctor,
    getBookingByDate,
    getBookingById,
    searchNamePatient,

    getPatientByBooking,
    getPatientInBooking,
    getBookingInPatient,
    deleteBooking
};

// function getBookingBySchedule(idDoctor,idSchedule) {
//     return axios({
//         method: 'GET',
//         url: `${Constants.SERVER}/doctors/${idDoctor}/bookings?filter={"where":{"scheduleId":"${idSchedule}"}}`,
//     });
// }

function getBookingByDoctor(idDoctor) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"doctorId":"${idDoctor}"},"order":["id DESC"]}`,
    });
}

function getBookingById(id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"id":"${id}"}}`,
    });
}

function getBooking(patientId) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"patientId":"${patientId}"},"order":["id DESC"]}`,
    });
}

function searchNamePatient(text) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings/searchNamePatient?name=${text}`,
    });
}

function getBookingByDate(date,id) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"date":"${date}","doctorId":"${id}"},"order":["time_start ASC"]}`,
    });
}

function getBookingByDoctorAnDay(id,date) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"doctorId":"${id}","date":"${date}"}}`,
    });
}
function postBooking(dataBooking) {
    const data = new URLSearchParams();
    data.append('phone',dataBooking.phone );
    data.append('old',dataBooking.old );
    data.append('anamnesis',dataBooking.anamnesis );
    data.append('name',dataBooking.name );
    data.append('email',dataBooking.email );
    data.append('sex',dataBooking.sex );
    data.append('note',dataBooking.note );
    data.append('date',dataBooking.date );
    data.append('time_start',dataBooking.time_start );
    data.append('time_end',dataBooking.time_end );
    data.append('follow_up_examination',dataBooking.follow_up_examination );
    data.append('patientId',dataBooking.patientId );
    data.append('doctorId',dataBooking.doctorId );
    data.append('re_examination_time',dataBooking.re_examination_time );
    data.append('times',dataBooking.times );
    return axios({
        method: 'POST',
        url: `${Constants.SERVER}/bookings`,
        data: data,
    });
}
function getPatientByBooking(idBooking) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings/${idBooking}/patient`,
    });
}

function getPatientInBooking(scheduleId,doctorId) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"scheduleId":"${scheduleId}","doctorId": "${doctorId}"}}`,
    });
}

function getBookingInPatient(idPatient) {
    return axios({
        method: 'GET',
        url: `${Constants.SERVER}/bookings?filter={"where":{"patientId":"${idPatient}"}}`,
    });
}

function deleteBooking(idBooking) {
    return axios({
        method: 'DELETE',
        url: `${Constants.SERVER}/bookings/${idBooking}`,
    });
}
