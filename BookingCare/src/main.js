﻿import App from "@/App.vue";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import router from "@/router";
import Vue from "vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import * as VueGoogleMaps from "vue2-google-maps";
import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';
import store from '@/store/index';
import { OverlayPlugin, CollapsePlugin, MediaPlugin, SidebarPlugin,VBTooltipPlugin   } from 'bootstrap-vue'

import LightBootstrap from './light-bootstrap-main'

Vue.use(LightBootstrap)

Vue.use(OverlayPlugin)
Vue.use(VBTooltipPlugin)
Vue.use(SidebarPlugin)
Vue.use(MediaPlugin)
Vue.use(CollapsePlugin)

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.use(Datetime)
Vue.config.productionTip = false;
Vue.use(BootstrapVue);

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDqW7nU-oIHsxrQnCsyPOhqULW9qCF0M10",
    libraries: "places"
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
