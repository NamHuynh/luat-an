import {notifyPatientService} from '@/services';

export default ({
    namespaced: true,
    state: {
        listDoctorResponsive: [],
        listNotify: [],
    },
    getters: {
    },
    mutations: {
        SET_LIST_DOCTOR: function (state, componentName) {
            state.modalComponent = componentName;
        },
        SET_LIST_NOTIFY: function (state, data) {
            state.listNotify =  data;
        }
    },
    actions: {
        setListNotify: async function (context) {
            await notifyPatientService.getNotify(localStorage.getItem("id"))
                .then(response => {
                    context.commit('SET_LIST_NOTIFY', response.data);
                })
                .catch(error => {
                    console.log(error);
                })
        },

    }
})
