export default ({
    namespaced: true,
    state: {
        stateChange: false
    },
    getters: {
    },
    mutations: {
        SET_USER: function(state,id,typeUser,email){
            state.id = id;
            state.typeUser = typeUser;
            state.email = email;
        },
        SET_STATE: function (state, stateChange) {
            state.stateChange = stateChange;
        }
    },
    actions: {
        setUser: function(context, id, typeUser, email){
            context.commit('SET_USER', id, typeUser,email);
        },
        setState: function (context, stateChange) {
            context.commit('SET_STATE', stateChange);
        }
    }
})
