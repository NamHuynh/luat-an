import Vue from 'vue';
import Vuex from 'vuex';
import bookingModule from './booking.store';
import authModule from './auth.store';
import doctorModule from './doctor.store';
import patientModule from './patient.store';
import timeModule from './Time.store';
import userModule from './user.store';
import specialistModule from "./specialict.store";
import notifyModule from "./notify-patient.store";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        booking: bookingModule,
        auth: authModule,
        doctor: doctorModule,
        patient: patientModule,
        time: timeModule,
        user: userModule,
        specialist: specialistModule,
        notify: notifyModule,
    },
});
